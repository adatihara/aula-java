package br.com.cursojava.comandos.controle;

public class TesteComandosRepeticao {
	public static void main(String[] args) {
		//Teste for
		// soma das pot�ncias de 2 de 0 at� 99
		Long resultado = 0l;
		for (int i = 0; i < 100; i++) {
			resultado += i*i;
		}
		System.out.println(resultado);
		
		String[] arrayStrings = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};
		for (String atual : arrayStrings) {
			System.out.println(atual);
			
		}	
	
		int contador = 0; 
		boolean variavelVerificacao = true;
		while(variavelVerificacao) {
			if (contador++ == 200) {
				variavelVerificacao = false;
			}
		}
		
		contador = 0; 
		while(true) {
			if (contador++ == 200) {
				break;
			}
		}
		
		contador = 0;
		boolean variavelVerificacaoDoWhile = false;
		do {
			contador++;
		} while (variavelVerificacaoDoWhile);
		
		System.out.println(contador);
	
		resultado = 0l;
		for (int i = 0; i < 100; i++) {
			if (i%5 == 0) {
				resultado += i*i;
			} else {
				continue;
			}
			System.out.println(resultado);
		}
	
	
	}
	
	
}
