package br.com.cursojava.comandos.controle.apoio;

public enum SwitchEnum {

	CASE_1(1, "Case String 1"),
	CASE_2(2, "Case String 2"),
	CASE_3(3, "Case String 3"),
	CASE_4(4, "Case String 4"),
	CASE_5(5, "Case String 5");
	
	SwitchEnum(Integer valor, String descricao){
		this.valor = valor;
		this.descricao = descricao;
	}
	
	private Integer valor;
	private String descricao;
	
	public Integer getValor(){
		return valor;
	}

	public String getDescricao(){
		return descricao;
	}

}
