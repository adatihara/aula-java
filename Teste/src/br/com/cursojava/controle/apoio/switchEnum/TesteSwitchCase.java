package br.com.cursojava.controle.apoio.switchEnum;

import br.com.cursojava.comandos.controle.apoio.SwitchEnum;

/**
 * M�todo que verifica o valor de enum e eecuta uma multiplica��o 
 * 
 * @param enumeravel
 * @author treina
 *
 */
public class TesteSwitchCase {
	public static Integer getEnumValue(SwitchEnum enumeravel) {
		Integer retorno = null;
		
		switch (enumeravel) {
		case CASE_1:
			retorno = enumeravel.getValor() * 40;
			break;
		case CASE_2:
			retorno = enumeravel.getValor() * 30;
			break;
		case CASE_3:
			retorno = enumeravel.getValor() * 20;
			break;
		case CASE_4:
			retorno = enumeravel.getValor() * 10;
			break;
		default:
			retorno = enumeravel.getValor() * 1;
			break;
		}
	
	return retorno;
	}
	/**
	 * M�todo que verifica o valor de enum e eecuta uma multiplica��o 
	 * 
	 * @param enumeravel
	 * @author treina
	 *
	 */

		public static Integer getIntegerValue(Integer inteiro) {
			Integer retorno = null;
			
			switch (inteiro) {
			case 10:
				retorno = 40;
				break;
			case 20:
				retorno = 30;
				break;
			case 30:
				retorno = 20;
				break;
			case 40:
				retorno = 10;
				break;

			default:
				retorno = 1;
				break;
			}
			
			return retorno;
		}
		/**
		 * M�todo que verifica o valor de enum e eecuta uma multiplica��o 
		 * 
		 * @param enumeravel
		 * @author treina
		 *
		 */
		
		public static String getStringValue(String string) {
			String retorno = null;
			
			switch (string) {
			case "A":
				retorno = "DCBA";
				break;
			case "AB":
				retorno = "CBA";
				break;
			case "ABC":
				retorno = "BA";
				break;
			case "ABCD":
				retorno = "A";
				break;
				
			default:
				retorno = "Valor n�o conhecido";
				break;
			}
			
			return retorno;
		}

		
		
		
		
		
		
		
		
		
		
		public static void main(String[] args) {
		//Teste para switch com eunmer�vel
		System.out.println(getEnumValue(SwitchEnum.CASE_1));
		System.out.println(getEnumValue(SwitchEnum.CASE_2));
		System.out.println(getEnumValue(SwitchEnum.CASE_3));
		System.out.println(getEnumValue(SwitchEnum.CASE_4));
		System.out.println(getEnumValue(SwitchEnum.CASE_5));
	
		//Teste para switch com inteiro
		System.out.println(getIntegerValue(10));
		System.out.println(getIntegerValue(20));
		System.out.println(getIntegerValue(30));
		System.out.println(getIntegerValue(40));
		System.out.println(getIntegerValue(70));

		
		//Teste para switch com inteiro
		System.out.println(getStringValue("A"));
		System.out.println(getStringValue("AB"));
		System.out.println(getStringValue("ABC"));
		System.out.println(getStringValue("ABCD"));
		System.out.println(getStringValue("ABCDE"));
		}
}
