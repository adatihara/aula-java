package br.com.cursojava.estuturadados.mapas;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;


public class TesteMapas {
	
	public static void main(String[] args) {

//		K de Key e V de value
//		Map<K, V>
		Map<String, Integer> mapaIdades = new HashMap<>();
		mapaIdades.put("Davi", 26);
		mapaIdades.put("David", 28);
		mapaIdades.put("Hendrel", 22);
		mapaIdades.put("Peterson", 33);
		mapaIdades.put("Rodrigo", 35);

		String[] nomes = {"Davi", "David", "Angelo", "Bruno", "Adriano", "Silvia", "Hendrel"};
		
		System.out.println("Procurar a idade das pessoas no mapa:");
		
		for (String nomeAtual :nomes) {
			if (mapaIdades.containsKey(nomeAtual)){
				System.out.println(String.format("A idade do %s � %d", nomeAtual, mapaIdades.get(nomeAtual)));
			}else {
				System.out.println("N�o conhe�o o " + nomeAtual);
			}
			
		}
	
	
		SortedMap<String, Integer> mapaIdadesOrdenados = new TreeMap<>();
		mapaIdadesOrdenados.put("Rodrigo", 35);
		mapaIdadesOrdenados.put("David", 28);
		mapaIdadesOrdenados.put("Davi", 26);
		mapaIdadesOrdenados.put("Peterson", 33);
		mapaIdadesOrdenados.put("Hendrel", 22);
		
		System.out.println("Procurar a idade das pessoas no mapa:");
		
		for (Entry<String, Integer> currentEntry : mapaIdadesOrdenados.entrySet()) {
			System.out.println(String.format("A idade do %s � %d",  currentEntry.getKey(), currentEntry.getValue()));
}		
		
	
	
	
	
	
	}
}
