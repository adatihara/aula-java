package br.com.cursojava.excecoes;

public class TesteExcecoes {
	
	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 */
	public static void testaPrimeiraException(Integer inteiro) throws MinhaPrimeiraException {
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
			
		}
		
		
	}

	/**
	 * Exemplo de coment�rio para lan�amento de exce��es
	 * @param inteiro
	 * @throws MinhaPrimeiraException
	 * @throws MinhaSegundaException 
	 */
	public static void testaHierarquiaDaPrimeiraException(Integer inteiro) throws MinhaPrimeiraException, MinhaSegundaException {
		if (inteiro % 5 == 0) {
			throw new MinhaPrimeiraException();
			
		}
		
		if (inteiro % 9 == 0) {
			throw new MinhaTerceiraException();
		}
		
		if (inteiro % 13 == 0) {
			throw new MinhaSegundaException();
		}
	}	

	public static void main(String[] args) {
//		try {
//			testaPrimeiraException(2);
//			testaPrimeiraException(15);
//		} catch (MinhaPrimeiraException e) {
//			System.out.println("Ocorreram erros no processamento.");
//		e,printStackTrace();
//		}
//	}

			try {
				testaHierarquiaDaPrimeiraException(16);
				testaHierarquiaDaPrimeiraException(5);
			} catch (Exception e) {
				System.out.println(e.getClass().toString());
				e.printStackTrace();
			}
			
			try {
				testaHierarquiaDaPrimeiraException(27);
			} catch (Exception e) {
				System.out.println(e.getClass().toString());
				e.printStackTrace();
			}
			
			testaTodasExceptions(5);
			testaTodasExceptions(54);
			testaTodasExceptions(63);
	
	} finally{
			System.out.println("Bloco finally sempre � executado");''
			
	}
}

	
		

