package br.com.cursojava.exemplo.classe;

import br.com.cursojava.exemplo.classe.abstrata.ExemploClasseAbstrata;

public class ExemploClasse {
	//classe concreta � uma classe que n�o tem nenhuma classe abstrata
	public static void main(String[] args) {
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.cadeiaDeCaracteres = "";
		exemploClasseComAtributos.umNumero = 0;
		
		//ExemploClasseAbstrata classeAbstrata = new ExemploClasseAbstrata();
		//claase abstrata n�o pode ser instanciada, somente ser extendiada 
		
	}
}
