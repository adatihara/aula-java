package br.com.cursojava.exemplo.classe;

public class ExemploClasseComAtributos {
	
	//Modificador Default
	String cadeiaDeCaracteres;
	
	//Modificador public
	public Integer umNumero;
	
	//Modificador private
	private Long umNumeroLong;

	public static void main(String[] args) {
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.umNumeroLong = 1l;  
	}
}
