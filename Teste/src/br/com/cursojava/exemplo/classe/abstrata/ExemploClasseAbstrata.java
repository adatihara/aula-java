package br.com.cursojava.exemplo.classe.abstrata;

public abstract class ExemploClasseAbstrata {
	//uma classe abstrata n�o permite ser istanciada dentro do java
	//essas classes tem um comportamento abstrato, todos tem movimento, mas nem todas da mesma forma, exemplo, passaro voa, cavalo corre
	//voc� sabe que existe mas n�o sabe como funciona
	public abstract void umMetodoAbstrato(String parametro);
	
}
