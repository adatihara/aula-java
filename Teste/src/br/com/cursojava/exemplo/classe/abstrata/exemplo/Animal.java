package br.com.cursojava.exemplo.classe.abstrata.exemplo;

public abstract class Animal {
	public abstract void move(Float distance);
}
