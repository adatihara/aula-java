package br.com.cursojava.exemplo.classe.abstrata.exemplo;

// extends significa que existe uma relação de generalização entre as classes
public class Person extends Animal {

	@Override
	public void move(Float distance) {
		System.out.println("Andei " + distance);
		// TODO Auto-generated method stub

	}

}
