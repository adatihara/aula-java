package br.com.cursojava.exemplo.classe.abstrata.exemplo;

public class TesteExemploClasseAbstrata {
	public static void main(String[] args) {
		Animal[] animalsArray = new Animal[2];
		
		animalsArray[0] = new Person();
		animalsArray[1] = new Fish();
		
		animalsArray[0].move(0.45f);
		animalsArray[1].move(1.5f);
	}
}
