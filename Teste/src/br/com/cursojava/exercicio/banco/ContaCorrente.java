package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

public class ContaCorrente extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		return BigDecimal.ZERO;
	}

	@Override
	public BigDecimal getIR() {
		return BigDecimal.ZERO;
	}

	@Override
	public Boolean validarLancamento(Lancamento lancamento) {
		Boolean res = getSaldo()
						.add(lancamento.getValor())
							.compareTo(BigDecimal.ZERO) >= 0;
		return res;
	}

}
