package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

public class ContaPoupanca extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		// TODO Auto-generated method stub
		return new BigDecimal("0.006687");
	}

	@Override
	public BigDecimal getIR() {
		// TODO Auto-generated method stub
		return new BigDecimal("0.15");
	}

	@Override
	public Boolean validarLancamento(Lancamento lancamento) {
		// TODO Auto-generated method stub
		Boolean res = getSaldo().add(lancamento.getValor()).compareTo(BigDecimal.ZERO) >= 0;
		return res;
	}

}
