package br.com.cursojava.operadoresbit;

public class TesteOperadoresBinarios {
	public static void main(String[] args) {
	
		int binario1111 = 15;
		
		int binario111 = binario1111 >> 1;
		int binario11110 = binario1111 << 1;
		System.out.println(binario111);
		System.out.println(binario11110);
		
		
		int binario1010 = 10;
		int binario0101 = 5;
		
		int binarioResultado1111 = binario1010 | binario0101;
		int binarioResultado0 = binario1010 & binario0101;
		System.out.println(binarioResultado1111);
		System.out.println(binarioResultado0);
		int resultadoDivisao = binario1010 / binario0101;
		System.out.println(resultadoDivisao);
		
	}
}
