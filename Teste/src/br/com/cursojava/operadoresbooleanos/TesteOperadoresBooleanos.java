package br.com.cursojava.operadoresbooleanos;

public class TesteOperadoresBooleanos {

	public static void main(String[] args) {
		
		boolean x, y, z, resultado;
		
		x = true;
		y = false;
		z = x;
		int a = 0,
			b = 1;
		
		// && AND
		resultado = x && z;
		System.out.println(x && y);
		System.out.println(resultado);
		
		
		// || OR
		resultado = x || z;
		System.out.println(x || y);
		System.out.println(resultado);
		
		
		// ! NOT
		resultado = !z;
		System.out.println(!z);
		System.out.println(resultado);
		
		// &  = AND (faz as duas compara��es sempre)
		// && = AND (faz a primeira compara��es e s� faz a segunda compara��o se a primeira for True)
		
		System.out.println(a == b & ++a <= b);
		System.out.println(a);
		
		a = 0;
		b = 1;
		// |  = OR (faz as duas compara��es)
		// || = OR (faz a primeira compara��es e s� faz a segunda compara��o se a primeira for True)
		System.out.println(a == b | ++a <= b);
		System.out.println(a);
	}
}
