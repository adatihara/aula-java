package br.com.cursojava.operadoresmatematicos;

import javax.annotation.processing.SupportedSourceVersion;

public class TesteOperadoresMatematicos {
	public static void main(String[] args) {
		long x, y, z;
		x = 0;
		y = 1;
		z = 2;
		
		// +
		System.out.println(x + y);
		
		// -
		System.out.println(y - z);
		
		// *
		System.out.println(y * z);
		
		// /
		System.out.println(20 / z);
		
		// % - MOD - resto da equa��o
		System.out.println(15 % z);
		
		// ++ depois ... usando o ++ depois, primeiro ele imprime, depois ele soma
		// ++ antes ... usando o ++ antes, primeiro ele soma e depois imprime
		System.out.println(x++);
		System.out.println(++x);
		
		// --
		System.out.println(z--);
		System.out.println(--z);
		
		
	}
}
