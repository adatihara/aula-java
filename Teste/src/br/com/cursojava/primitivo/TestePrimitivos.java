package br.com.cursojava.primitivo;

import javax.annotation.processing.SupportedSourceVersion;

public class TestePrimitivos {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int primitivoInteiro = 5;
		int primitivoInteiroSemValor = 0;
		
		System.out.println(primitivoInteiro);
		System.out.println(primitivoInteiroSemValor);
		
		char primitivoCaractere = 'a';
		 
		System.out.println(primitivoCaractere);
		
		boolean primitivoBooleano = false;
		
		System.out.println(primitivoBooleano);
		
		long primitivoLong = 2;
		System.out.println(primitivoLong);
		
		Integer wrapperInteiro = 2;
		System.out.println(wrapperInteiro);
		
		Character wrapperCaractere = 'a';
		System.out.println(wrapperCaractere);
		
		Boolean wrapperBoleano = true;
		System.out.println(wrapperBoleano);
		
		Long wrapperLong = 20l;
		System.out.println(wrapperLong);
		
		System.out.println(wrapperInteiro.getClass());
		
		
		String[] arrayStrings = new String[10];
		arrayStrings[0] = "Curso Java";
		System.out.println(arrayStrings[0]);
		System.out.println(arrayStrings[1]);
		System.out.println(arrayStrings);
		System.out.println(arrayStrings.toString());
		System.out.println(arrayStrings.toString());
		
		// Teste operadores compara��o
		// Igual
		boolean testeIgual = 2 == 3;
		// diferente
		boolean testeDiferente = 2 != 3;
		//maior
		boolean testeMenor = 2 < 3;
		//maior igual
		boolean testeMenorIgual = 2 <= 2;
		//maior
		boolean testeMaior = 2 > 3;
		//maior igual
		boolean testeMaiorIgual = 2 >= 2;
		
		System.out.println(testeIgual);
		System.out.println(testeDiferente);
		System.out.println(testeMenor);
		System.out.println(testeMenorIgual);
		System.out.println(testeMaior);
		System.out.println(testeMaiorIgual);
		
		//Operadores de igual (==) e diferente (!=) tamb�m sao aplicaveis
		TesteReferencia diferente = new TesteReferencia();
		diferente.conteudo = "1";
		
		TesteReferencia igual = new TesteReferencia();
		diferente.conteudo = "1";
		
		System.out.println(diferente);
		System.out.println(igual);
		
		boolean testeIgualObjetos = diferente == igual;
		System.out.println(testeIgualObjetos);
		boolean testeDiferenteObjetos = diferente != igual;
		System.out.println(testeDiferenteObjetos);
		
		
	}
}
