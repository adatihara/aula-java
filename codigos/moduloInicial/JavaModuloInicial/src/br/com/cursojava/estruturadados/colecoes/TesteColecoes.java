package br.com.cursojava.estruturadados.colecoes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import br.com.cursojava.exemplo.classe.abstrata.exemplo.Person;

public class TesteColecoes {

	public static void main(String[] args) {
		
//		Código lança erro porque não tem verificação de conversão de tipo
//		List listaObjetos = new ArrayList();
//		listaObjetos.add("Teste");
//		listaObjetos.add("    Teste 2   ");
//		listaObjetos.add(new Person());
//
//		for (Object currentObject : listaObjetos) {
//			String s = (String) currentObject;
//			System.out.println(s.trim());
//		}
//		
		
		List<String> listaStrings = new ArrayList<>();
		listaStrings.add("tomate");
		listaStrings.add("maça");
		listaStrings.add("abobora");
		listaStrings.add("melancia");
		listaStrings.add("abobrinha");
		listaStrings.add("limao");
		listaStrings.add("abobrinha");
//		listaStrings.add(1l); Erro porque long não é do tipo string
		
		//ordenação e reversão da lista
		Collections.sort(listaStrings);
		Collections.reverse(listaStrings);
		Collections.shuffle(listaStrings);
		
		System.out.println("Teste de Lista de strings com generics:");
		
		for (String currentString : listaStrings) {
			System.out.println(currentString);
		}

		
		Set<Integer> conjuntoInteiros = new HashSet<Integer>();
		conjuntoInteiros.add(1);
		conjuntoInteiros.add(1);
		conjuntoInteiros.add(2);
		conjuntoInteiros.add(3);
		conjuntoInteiros.add(4);
		conjuntoInteiros.add(5);
		conjuntoInteiros.add(5);

		System.out.println("Teste de set com generics:");
		for (Integer currentInteger : conjuntoInteiros) {
			System.out.println(currentInteger);
		}
		

		
		Set<Integer> conjuntoInteirosOrdenados = new TreeSet<Integer>();
		conjuntoInteirosOrdenados.add(5);
		conjuntoInteirosOrdenados.add(3);
		conjuntoInteirosOrdenados.add(4);
		conjuntoInteirosOrdenados.add(2);
		conjuntoInteirosOrdenados.add(2);
		conjuntoInteirosOrdenados.add(9);
		conjuntoInteirosOrdenados.add(5);

		System.out.println("Teste de sorted set com generics:");
		for (Integer currentInteger : conjuntoInteirosOrdenados) {
			System.out.println(currentInteger);
		}
		
		
	}
	
	
}
