package br.com.cursojava.estruturadados.mapas;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

public class TesteMapas {

	public static void main(String[] args) {
		
		Map<String, Integer> mapaIdades = new HashMap<>();
		mapaIdades.put("Davi", 26);
		mapaIdades.put("David", 28);
		mapaIdades.put("Hendrel", 22);
		mapaIdades.put("Peterson", 33);
		mapaIdades.put("Rodrigo", 35);
		
		
		String[] nomes = {"Davi", "David", "Angelo", "Bruno", "Adriano", "Silvia", "Hendrel"};
		
		System.out.println("Procurar a idade das pessoas no mapa:");
		for (String nomeAtual : nomes) {
			if (mapaIdades.containsKey(nomeAtual)) {
				System.out.println(String.format("A idade do %s é %d", nomeAtual, mapaIdades.get(nomeAtual)));
			} else {
				System.out.println("Não conheço o " + nomeAtual);
			}
		}
		
		
		SortedMap<String, Integer> mapaIdadesOrdenado = new TreeMap<>();
		mapaIdadesOrdenado.put("Rodrigo", 35);
		mapaIdadesOrdenado.put("David", 28);
		mapaIdadesOrdenado.put("Davi", 26);
		mapaIdadesOrdenado.put("Peterson", 33);
		mapaIdadesOrdenado.put("Hendrel", 22);
		

		System.out.println("imprimir todos os valores do mapa:");
		for (Entry<String, Integer> currentEntry : mapaIdadesOrdenado.entrySet()) {
			System.out.println(String.format("A idade do %s é %d", currentEntry.getKey(), currentEntry.getValue()));
		}
	}
	
	
}
