package br.com.cursojava.exemplo.classe;

public class ExemploClasse {
	
	public static void main(String[] args) {
		ExemploClasseComAtributos exemploClasseComAtributos = new ExemploClasseComAtributos();
		exemploClasseComAtributos.cadeiaDeCaracteres = "";
		exemploClasseComAtributos.umNumero = 0;
		
		//ExemploClasseAbstrata classeAbstrata = new ExemploClasseAbstrata(); 
		// Classe abstrata não pode ser instanciada, apenas extendida
	}
}
