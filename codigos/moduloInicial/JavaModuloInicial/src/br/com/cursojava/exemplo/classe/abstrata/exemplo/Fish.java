package br.com.cursojava.exemplo.classe.abstrata.exemplo;

public class Fish extends Animal {

	@Override
	public void move(Float distance) {
		System.out.println("Nadei " + distance);
	}

}
