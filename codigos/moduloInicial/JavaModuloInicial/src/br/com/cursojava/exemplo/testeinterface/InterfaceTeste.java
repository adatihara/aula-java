package br.com.cursojava.exemplo.testeinterface;

public interface InterfaceTeste {

	public void metodoA();
	
	public String metodoB();
	
	public Integer[] metodoC();
	
	
}
