package br.com.cursojava.exercicio.banco;

import java.util.ArrayList;
import java.util.List;

import br.com.cursojava.exercicio.banco.exceptions.ClienteNaoEncontradoException;

public class BancoClientes {

	private List<Cliente> clientes;

	public BancoClientes() {
		super();
		Conta cc1 = new ContaCorrente();
		cc1.setNumeroConta(123);
		cc1.setNumeroDigito(1);
		cc1.setBloqueio(true);
		
		Conta cc2 = new ContaCorrente();
		cc2.setNumeroConta(1234);
		cc2.setNumeroDigito(1);
	
		Conta cc3 = new ContaCorrente();
		cc3.setNumeroConta(12345);
		cc3.setNumeroDigito(1);
		
		Conta cp1 = new ContaPoupanca();
		cp1.setNumeroConta(563);
		cp1.setNumeroDigito(1);
		
		Conta cp2 = new ContaPoupanca();
		cp2.setNumeroConta(125);
		cp2.setNumeroDigito(1);
		cp2.setBloqueio(true);
		
		Conta cp3 = new ContaPoupanca();
		cp3.setNumeroConta(896);
		cp3.setNumeroDigito(1);
		

		
		Conta ci1 = new ContaInvestimento();
		ci1.setNumeroConta(2548);
		ci1.setNumeroDigito(1);
		
		Conta ci2 = new ContaInvestimento();
		ci2.setNumeroConta(1249);
		ci2.setNumeroDigito(1);
		
		Conta ci3 = new ContaInvestimento();
		ci3.setNumeroConta(687);
		ci3.setNumeroDigito(1);
		
		
		Cliente cliente1 = new Cliente();
		cliente1.setIdentificadorCliente(1);
		cliente1.getContasCliente().add(cc1);
		cliente1.getContasCliente().add(cp1);
		cliente1.getContasCliente().add(ci1);
		
		Cliente cliente2 = new Cliente();
		cliente2.setIdentificadorCliente(2);
		cliente2.getContasCliente().add(cc2);
		cliente2.getContasCliente().add(cp2);
		cliente2.getContasCliente().add(ci2);
		
		Cliente cliente3 = new Cliente();
		cliente3.setIdentificadorCliente(3);
		cliente3.getContasCliente().add(cc3);
		cliente3.getContasCliente().add(cp3);
		cliente3.getContasCliente().add(ci3);
		
		clientes = new ArrayList<Cliente>();
		clientes.add(cliente1);
		clientes.add(cliente2);
		clientes.add(cliente3);
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	
	public Cliente findCliente (Integer identificadorCliente) throws ClienteNaoEncontradoException {
		Cliente res = null;
		for (Cliente cliente : clientes) {
			if (identificadorCliente.equals(cliente.getIdentificadorCliente())) {
				res = cliente;
				break;
			}
		}
		
		if (null == res) {
			throw new ClienteNaoEncontradoException("Não encontrei o cliente com ID: " + identificadorCliente);
		}
		
		return res;
	}
	
	
}