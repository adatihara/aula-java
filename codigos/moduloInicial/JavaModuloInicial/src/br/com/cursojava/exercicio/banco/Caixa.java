package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Caixa {

	private static BancoClientes bancoClientes = new BancoClientes();
	
	public static Boolean lancar(Integer identificadorCliente,
								 Integer numeroConta,
								 Integer digitoConta,
								 BigDecimal valor,
								 Date dataLancamento) {
		Boolean res = Boolean.FALSE;
		for (Cliente clienteAtual : bancoClientes.getClientes()) {
			if (clienteAtual.getIdentificadorCliente().equals(identificadorCliente)) {
				for (Conta contaAtual : clienteAtual.getContasCliente()) {
					if (contaAtual.getNumeroConta().equals(numeroConta) && 
						contaAtual.getNumeroDigito().equals(digitoConta)) {
						res = contaAtual.realizarLancamento(valor, dataLancamento);
					}
				}
			}
		}
		return res;
	}
	
	
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date data1 = new Date();
		Date data2 = format.parse("15/02/2014");
		Date data3 = format.parse("15/02/2012");
		Date data4 = new Date();
		Date data5 = format.parse("15/06/2014");
		Date data6 = format.parse("15/09/2014");
		Date data7 = new Date();
		Date data8 = format.parse("15/01/2012");
		Date data9 = format.parse("15/06/2013");
		
		lancar(1, 123, 1, new BigDecimal(10000), data1);
		lancar(2, 1234, 1, new BigDecimal(8500), data2);
		lancar(3, 12345, 1, new BigDecimal(1800), data3);
		lancar(1, 563, 1, new BigDecimal(18000000), data4);
		lancar(2, 125, 1, new BigDecimal(1520), data5);
		lancar(3, 896, 1, new BigDecimal(169.62), data6);
		lancar(1, 2548, 1, new BigDecimal(96.58), data7);
		lancar(2, 1249, 1, new BigDecimal(12578), data8);
		lancar(3, 687, 1, new BigDecimal(12), data9);
	}
	
}
