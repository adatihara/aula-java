package br.com.cursojava.exercicio.banco;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

	private Integer identificadorCliente;
	
	private List<Conta> contasCliente;

	public Cliente(Integer identificadorCliente, List<Conta> contasCliente) {
		super();
		this.identificadorCliente = identificadorCliente;
		this.contasCliente = contasCliente;
	}

	public Cliente() {
		super();
		this.contasCliente = new ArrayList<Conta>();
	}

	public Integer getIdentificadorCliente() {
		return identificadorCliente;
	}

	public void setIdentificadorCliente(Integer identificadorCliente) {
		this.identificadorCliente = identificadorCliente;
	}

	public List<Conta> getContasCliente() {
		return contasCliente;
	}

	public void setContasCliente(List<Conta> contasCliente) {
		this.contasCliente = contasCliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contasCliente == null) ? 0 : contasCliente.hashCode());
		result = prime * result + ((identificadorCliente == null) ? 0 : identificadorCliente.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (contasCliente == null) {
			if (other.contasCliente != null)
				return false;
		} else if (!contasCliente.equals(other.contasCliente))
			return false;
		if (identificadorCliente == null) {
			if (other.identificadorCliente != null)
				return false;
		} else if (!identificadorCliente.equals(other.identificadorCliente))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [identificadorCliente=" + identificadorCliente + ", contasCliente=" + contasCliente + "]";
	}
	
}
