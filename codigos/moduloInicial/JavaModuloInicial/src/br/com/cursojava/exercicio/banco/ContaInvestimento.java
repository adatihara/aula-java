package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

public class ContaInvestimento extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		return new BigDecimal("0.015");
	}

	@Override
	public BigDecimal getIR() {
		return new BigDecimal("0.115");
	}

	@Override
	public Boolean validarLancamento(Lancamento lancamento) {
		Boolean res = getSaldo().add(lancamento.getValor()).compareTo(BigDecimal.ZERO) >= 0;
		return res;
	}

}
