package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;

public class ContaPoupanca extends Conta {

	@Override
	public BigDecimal getTaxaRendimento() {
		return new BigDecimal("0.006687");
	}

	@Override
	public BigDecimal getIR() {
		return new BigDecimal("0.15");
	}

	@Override
	public Boolean validarLancamento(Lancamento lancamento) {
		Boolean res = getSaldo().add(lancamento.getValor()).compareTo(BigDecimal.ZERO) >= 0;
		return res;
	}

}
