package br.com.cursojava.exercicio.banco;

import java.math.BigDecimal;
import java.util.Date;

public class Lancamento {

	private Date data;
	private BigDecimal valor;
	
	public Lancamento() {
		super();
	}

	public Lancamento(Date data, BigDecimal valor) {
		super();
		this.data = data;
		this.valor = valor;
	}
	
	/**
	 * Saldo a partir de um lancamento
	 * lancamento * (1 + taxaRendimento ^ (diferenca Datas))
	 * decrescido do IR
	 * 
	 * @return saldo atual
	 */
	public BigDecimal getValorAtualizado(BigDecimal taxaRendimento, BigDecimal ir) {
		BigDecimal valorLancamentoAtualizado = valor.multiply(BigDecimal.ONE.add(taxaRendimento).pow(getDiferencaDatas()));
		BigDecimal valorJuros = valorLancamentoAtualizado.subtract(valor);
		BigDecimal valorJurosDecrescidoIR = valorJuros.subtract(valorJuros.multiply(ir));
		return valor.add(valorJurosDecrescidoIR);
	}

	private int getDiferencaDatas() {
		Long diferencaEmMilis = (new Date()).getTime() - data.getTime();
		diferencaEmMilis /= (1000L * 60L * 60L * 24L * 30L);
		return diferencaEmMilis.intValue();
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lancamento other = (Lancamento) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lancamento [data=" + data + ", valor=" + valor + "]";
	}
	
	
	
	
}
