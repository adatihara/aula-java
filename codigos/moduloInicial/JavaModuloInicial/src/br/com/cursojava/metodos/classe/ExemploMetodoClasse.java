package br.com.cursojava.metodos.classe;

public class ExemploMetodoClasse {

	public static Integer potencia2(Integer base) {
		return base * base;
	}
	
	
	public static void main(String[] args) {
		System.out.println(ExemploMetodoClasse.potencia2(4));
	}
	
}
