package br.com.cursojava.operadores.booleanos;

public class TesteOperadoresBooleanos {

	public static void main(String[] args) {
		boolean x, y, z, resultado;
		x = true;
		y = false;
		z = x;
		
		int a = 0, 
			b = 1;
		
		// && AND
		resultado = x && z;
		System.out.println(x && y);
		System.out.println(resultado);
		// || OR
		resultado = x || z;
		System.out.println(x || y);
		System.out.println(resultado);
		
		//  ! NOT
		resultado = !z;
		System.out.println(!y);
		System.out.println(resultado);
		
		// & AND (faz as duas comparações)
		System.out.println(a == b & ++a <= b);
		System.out.println(a);
		
		a = 0; 
		b = 1;
		// | OR (faz as duas comparações)
		System.out.println(a == --b | ++a <= b);
		System.out.println(a);
		
		
	}
	
}
