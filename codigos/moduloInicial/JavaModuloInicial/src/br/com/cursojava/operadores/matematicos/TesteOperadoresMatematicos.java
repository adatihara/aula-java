package br.com.cursojava.operadores.matematicos;

public class TesteOperadoresMatematicos {

	public static void main(String[] args) {
		long x,y,z;
		x = 0;
		y = 1;
		z = 2;
		
		
		// +
		System.out.println(x + y);
		// -
		System.out.println(y - z);
		// *
		System.out.println(y * z);
		// /
		System.out.println(20 / z);
		// %
		System.out.println(15 % z);
		// ++
		System.out.println(x++);
		System.out.println(++x);
		// --
		System.out.println(z--);
		System.out.println(--z);
	}
	
}
